Source: andi
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Fabian Klötzl <kloetzl@evolbio.mpg.de>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 12),
               libdivsufsort-dev,
               pkgconf,
               libgsl-dev,
               libglib2.0-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/andi
Vcs-Git: https://salsa.debian.org/med-team/andi.git
Homepage: https://github.com/EvolBioInf/andi

Package: andi
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libdivsufsort3
Description: Efficient Estimation of Evolutionary Distances
 This is the andi program for estimating the evolutionary distance
 between closely related genomes. These distances can be used to rapidly
 infer phylogenies for big sets of genomes. Because andi does not compute
 full alignments, it is so efficient that it scales even up to thousands
 of bacterial genomes.
